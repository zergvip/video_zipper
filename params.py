import sys
import os.path

class params:

	def __init__(self , args ):
		
		self.startNumber = 1
		self.onlyMD5summ = False
		self.d = False

		if len( args ) < 2:
		    print('Не указано путь источника файлов')
		    sys.exit()
		else:
		    print( 'Рабочий путь: ' + args[1] )
		    self.path = format( args[1] )
		    if not os.path.exists( self.path ):
		    	print( ' - Не найден путь источника файлов ' , self.path )
		    	sys.exit()


		self.exeptionList = ""
		for i in range( 0 , len(args) ):

			# Обработка парамтера -m, только посчитать контрольыне суммы ничего не трогать
		    if args[i] == "-m":
		        print(" + Режим подсчета контрольных сумм. Файлы обрабатываться не будут, будут получены и сохранены контольные суммы файлов")
		        self.onlyMD5summ = True

		    # Обработка парамтера -d, контрольные суммы файлов обработка которых была не эффективна будете добалвена с файл checksum.txt
		    if args[i] == "-d":
		        print(" + Указан параметр -d. Контрольные суммы файлов обработка которых была не эффективна будет добалвена с файл checksum.txt")
		        self.d = True

			# Обработка парамтера -n, который указывает с какого файла начинать
		    if args[i] == "-n":
		        try:
		            print(" + Указан номер файла по списку " + str( args[i+1] ) )
		            self.startNumber = int(args[i+1])
		        except:
		            print(" - Ошибка в синтаксисе параметра -n" )
		            sys.exit()

		    if args[i] == "-e":
		        try:
		            print("Указан список исключений: " + str( args[i+1] ) )
		            self.exeptionList = args[i+1].split(",")

		            for ex in self.exeptionList:		            
		            	if not os.path.exists( ex ):
		            		print( " - Путь исключений не найден" )
		            		exit()

		            print(" + Список исключений верный, все указаныне пути существуют") 		
		        
		        except Exception as e:
		            print(" - Ошибка в списке исключений" + e  )
		            exit()
		            
		   

