import shutil
from processedfile import processedFile
import os
import multiprocessing
import subprocess
from subprocess import check_output, CalledProcessError, STDOUT
import traceback


class videoFile( processedFile ):


	def checkType( self ):

		file = self.file		

		if file.lower().endswith(".m4v") or file.lower().endswith(".mts") or file.lower().endswith(".wmv") or file.lower().endswith(".vob") or file.lower().endswith(".flv") or file.lower().endswith(".avi") or file.lower().endswith(".mpeg") or file.lower().endswith(".mp4") or file.lower().endswith(".mov") or file.lower().endswith(".mkv") or file.lower().endswith(".3gp") or file.lower().endswith(".h264") or file.lower().endswith(".mpg") or file.lower().endswith(".asx") or file.lower().endswith(".asf") or file.lower().endswith(".f4v") or file.lower().endswith(".3gp2") or file.lower().endswith(".swf") or file.lower().endswith(".ogm"):
			
			return True

		else:			
			return False

	def zipFile(self):

		try:

			if not os.path.exists( self.scriptPath + str("/input/" + self.file) ):
				self._log.error( " - Не найден файл для сжатия в папке input ")
				return False

			self._log.info( "ffmpeg -hide_banner -y -i \"" + self.scriptPath + "/input/" + self.file + "\" -max_muxing_queue_size 5000 -vcodec  libx264 -crf 20 -preset veryslow -profile:v baseline \"" + self.scriptPath + "/output/" + self.file + "\""  )
			
			DNULL = open( os.devnull , "w" )
			st = subprocess.call(  "ffmpeg -hide_banner -y -i \"" + self.scriptPath + "/input/" + self.file + "\" -max_muxing_queue_size 5000 -vcodec  libx264 -crf 20 -preset veryslow -profile:v baseline \"" + self.scriptPath + "/output/" + self.file + "\""   , stdout = DNULL , shell=True )

		except Exception as e:
			self._log.error( " - Исключение во время сжатия " + str( e ) )
			return False;

		return True;


	def iszipFile(self):

		if not os.path.exists( self.scriptPath + str("/output/" + self.file) ):
		    self._log.error( " - Не найден сжатый файл ")
		    return 0

		try:
		    tmpStats = os.stat( self.scriptPath + str("/output/" + self.file) )
		except:
		    self._log.error( " - Не удалось прочитать размер сжатого файла ")
		    return 0

		if tmpStats.st_size == 0:
			self._log.error( " - Размер сжатого файла 0 ")
			return 0


		# Получаем длительность до сжатия
		try:
			inputF = check_output( "ffmpeg  -i \"" +  self.scriptPath + str("/input/") + str( self.file ) + "\" 2>&1 | grep Duration:"  , shell=True, universal_newlines=True, stderr=STDOUT , text=True )
		except Exception as ex:
			self._log.error( " - Ошибка при получении длительности исходящего файла " + str( ex ) )
			return 0

		#	Получаем дилтельность после сжатия
		try:
			outputF = check_output( "ffmpeg  -i \"" +  self.scriptPath + str("/output/") + str( self.file ) + "\" 2>&1 | grep Duration:"  , shell=True, universal_newlines=True, stderr=STDOUT  , text=True )
		except Exception as ex:
			self._log.error( " - Ошибка при получении длительности сжатого файла " +  str( ex ) )
			return 0

		try:
			#	Парсим
			indexInput = inputF.find("Duration: ")
			inputTime = ( int( inputF[ indexInput + 10 ] ) * 60 * 60 * 10 ) + ( int(inputF[ indexInput + 11 ]) * 60 * 60 ) + ( int(inputF[ indexInput + 13 ]) * 60  * 10 ) + ( int(inputF[ indexInput + 14 ]) * 60  ) + ( int(inputF[ indexInput + 16 ])  * 10 ) +  int( inputF[ indexInput + 17 ] )

			indexOutput = outputF.find("Duration: ")
			outputTime = ( int(outputF[ indexOutput + 10 ]) * 60 * 60 * 10 ) + ( int(outputF[ indexOutput + 11 ]) * 60 * 60 ) + ( int(outputF[ indexOutput + 13 ]) * 60  * 10 ) + ( int(outputF[ indexOutput + 14 ]) * 60  ) + ( int(outputF[ indexOutput + 16 ])  * 10 ) + ( int(outputF[ indexOutput + 17 ])  )

			self._log.info("Длительность Видео до сжатия " + str(inputTime))
			self._log.info("Длительность Видео после сжатия " + str(outputTime))
		except Exception as ex:
			self._log.error( " - Не удалось получить длительность для сравнения: " + str(ex))
			return 0

		#	Сранвиаем
		# 1 секунду оставим на погрешность.
		if abs ( inputTime - outputTime ) > 1 :
		    self._log.infoConsole(" - ВНИМАНИЕ! Длительность файлов до сжатия и после не совпадает")
		    return 0

		return 1

	
