import sys
import os.path
import shutil
import traceback

import logging
from processedfile import processedFile
from md5summ import md5summ
from myLog import myLog
from saveExit import saveExitClass
from statistics import myStat 
from params import params
from fsOperations import fsOperations
from pocedureCompress import pocedureCompress

from videoFile import videoFile


try:
    _log = myLog()
except:
    print(' - Ошибка инициализации логирования:\n' + traceback.format_exc())
    sys.exit()

_log.infoConsole("Версия: 0.6.2")

#   Создаем обьект статистики
stat = myStat(_log)
showStat = True

# Инициализируем обработчик нажатий клавиш CTRL Z / C
SE = saveExitClass()

if os.path.exists("lock"):
    print("ВНИМАНИЕ!!! обнаружен lock файл! Предыдущий запуск программы завершился серьезной ошибкой. Информацию ищите в логах. Для корректного запуска проагрммы удалите lock файл.")
    os._exit(0)

# Обрабатываем параметры
params = params(sys.argv)
print("ВНИМАНИЕ!!! Для корректного завершения прогарммы испольуйзте комбинации CTRL C и CTRL Z")
input("Нажмите Enter для старта...")

if params.startNumber > 1 :
	print("Проускаю файлы ожидайте...")

# подкгржаем базу контрольных сумм
md5 = md5summ(_log)
md5.load()

# Операции с файловой системой
fs = fsOperations(_log)
fs.initTmpDirs()

supportedFiles = [ videoFile() ]   

for root, dirs, files in os.walk(params.path):
    for file in files:        

        #   Перебираем поддерживаемые типы и определяем
        #   сможет ли кто т обработать текущий тип
        nextFile = False
        for currentFile in supportedFiles:
            currentFile.setFile( root , file );
            #   Ищем
            if not currentFile.checkType():
                nextFile = True
                break
        
        if nextFile:
            continue

        # стартуем статистику
        stat.startNewFile()    

        # обработка параметра -n, котоырй указывает с каког офайла наичнать. До n все файлы будут пропущены
        if stat.allfiles < params.startNumber: 
            stat.fileAlreadyCompress()           
            continue
        if not showStat:
            stat.showInfo()

        showStat = False

        #   Наичнаем процедуру сжатия
        procCompress = pocedureCompress( currentFile , _log , stat )       

        #   Проверим нет ои файла в спсике исключений
        if procCompress.exeptionCheck( params ):
            _log.infoConsole (" - Файл в спсике исключений, пропускаю " )
            continue 

        #   Получаем размер
        if not procCompress.getSize():
            _log.error(" - Не смог получить размер файла, пропускаю ")
            continue

        #   Нулевой размер файла
        if currentFile.srcStats.st_size == 0:
            _log.error(" - Файл имеет нулевой рамер, пропускаю")
            continue

        print("Размер: " + str( round( currentFile.srcStats.st_size / 1024 / 1014 , 2 ) ) + " Мб" )

        #   Чистим tmp
        fs.initTmpDirs()

        # Компируем файл во временну юпапку input    
        if not procCompress.loadToInput():
            _log.error(" - Не удалось скачать файл во временную папку, пропускаю")
            continue    

        #   Получаем контрольную сумму файла
        if not procCompress.getCheckSumm( md5 ):
            _log.error(" - Ошибка получения контрольной суммы, пропускаю")
            continue    

        #   Ищем совпадение в контрольнйо сумме
        if not procCompress.checkCheckSumm( md5 ):
            _log.infoConsole(" + Файл с данной контрольнойсуммой ( " + str(md5.lastmd5Summ) + " ) уже обрабатывался, пропускаю  " )
            stat.fileAlreadyCompress()
            continue

        # Если был указан параметр -m в этом месте мы должны записать контрольную сумму файала и все
        if params.onlyMD5summ:
            
            if not md5.get( currentFile.scriptPath + "/input/" + currentFile.file ):
                continue

            md5.add( md5.lastmd5Summ )

            _log.infoConsole(" + Контрольная сумма успешно получена и сохранена " )
            # Файл считаеттся успешно обработанным
            stat.filecount += 1
            continue

        #   Сжимаем файл    
        if not procCompress.compressFile():
            _log.error(" - Ошибка во время сжатии файла, пропускаю" )
            continue
            
        if not procCompress.checkCompressFile():
            _log.error(" - Ошибка при сжатии файла, пропускаю" )
            continue


        #   сравниваем размеры сжатого и исходного файлов
        if not currentFile.calulateSizes():
            _log.error(" - Ошибка при обработке размеров файла, пропускаю" )
            continue


        if currentFile.compareFiles():
            _log.infoConsole(" + Сжатый весит меньше, заменяем")

            #   Удаялем исходный файл
            if not procCompress.removeSrcFile():
                _log.error(" - Не смогли удалить исходный файл, пропускаю")
                continue

            #   Критично важный момент
            #   Запрещаем сейчас закрывать программу иначе мы потеряем данные
            SE.isCanExit(False)

            if not procCompress.uploadCompressedFile():
                #   мы не смогли залить обратно файл!!! что то случилось,
                #   останавлвиаем программу не в коем случае не удаляем временный файл в папке input
                #   иначе мы окончательно потеряем этот файл
                _log.error(" - НЕ СМОГЛИ СКОПИРОВАТЬ СЖАТЫЙ ФАЙЛ, НО ИСХОДЫНЙ УЖЕ УДАЛЕН!!! ОСТАНОВКА ПРОГРАММЫ. Исходный файл находиться в папке input. ПОВТОРОНЫЙ ЗАПУСК ПРОГРАММЫ ПРИВЕДЕТ К ПОТЕРЕ файла")
                fs.mkLockFile()
                os._exit(0)
                continue

            #   Разрешаем закрывать программу
            SE.isCanExit(True)

        else:
            stat.compressNotEffective += 1
            _log.infoConsole(" - Сжатый весит больше, не трогаем, пропускаем")

            # Если параметр d True то сохраняем контрольную сумму
            if params.d:

                _log.infoConsole(" - Указан параметр d, занчит текущий файл всеравно будет записан в файл конрольных сумм и в дальнейшем не будет обрабатываться")

                if not procCompress.saveMD5Self( md5 ):
                    _log.error(" - Не удалось записать контрольную сумму файла. Возникла непредвтденная ошибка")


            continue

        if not procCompress.saveMD5( md5 ):
            _log.error(" - Не удалось записать контрольную сумму файла. Возникла непредвтденная ошибка")
            continue            
       
        # Все прошло отлично подсчитываем размеры
        # записываем контрольную сумму           

        stat.fileCompressGood( int(currentFile.inputStats.st_size) , int(currentFile.outputStats.st_size) )


_log.infoConsole(str("\n + ГОТОВО"))

stat.allfiles +=1
stat.showInfo()

