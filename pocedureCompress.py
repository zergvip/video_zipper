from fsOperations import fsOperations
import os
import traceback
import time

class pocedureCompress:

	def __init__( self , processedFile ,  _log , stat):
		self.processedFile = processedFile		

		self.processedFile.setLog(_log)
		self.processedFile.setStat(stat)

		self.stat = stat

		# колчиестов попыток
		self.tryCountLimit = 2

		#   Задержка между попытками в секундах
		self.sleepTryLimit = 5

		self.fs = fsOperations( _log )

		_log.infoConsole ("\nФайл: " + os.path.join( self.processedFile.root , self.processedFile.file ) )
		self._log = _log

	def zipFile(self):
		print(" - Не переопределен метод zipFile")
		os.exit(1)	

	#   Проверим не в списке ли исключений файл
	def exeptionCheck( self , params ):    
	    
		for ex in params.exeptionList:
			if os.path.join( self.processedFile.root , self.processedFile.file ).startswith(ex.strip()):
				self.stat.exep += 1
				return True
		
		return False

	def getSize( self ):
		
		# пробуем получить размер
        # - 1 если файл нулевой иначе 0 будет воспринято как False
        
		tryCount = 0

		while self.processedFile.getFileSize() < 0:
		    tryCount += 1		   
		    time.sleep( self.sleepTryLimit )
		    if tryCount > self.tryCountLimit:
		        return False

		    print( "Попытка № " + str(tryCount) )    

		return True 

	def loadToInput(self):
		
		tryCount = 0
		#print("Копирую " , end=" ", flush = True)
		while not self.fs.copyFile( self.processedFile.filePath , self.processedFile.scriptPath + "/input/" + self.processedFile.file ):
			tryCount += 1		    
			time.sleep( self.sleepTryLimit )
			if tryCount > self.tryCountLimit:
				return False

			print( "Попытка № " + str(tryCount))

		return True

	def getCheckSumm( self , md5 ):
	
		# пробдуем пролучить контрольную сумму файла
		tryCount = 0
		while not md5.get( self.processedFile.scriptPath + "/input/" + self.processedFile.file ):
			tryCount += 1			
			time.sleep( self.sleepTryLimit )
			if tryCount > self.tryCountLimit :
				os.remove( self.processedFile.scriptPath + "/input/" + self.processedFile.file )
				return False
			print( "Попытка № " + str(tryCount) )

		return True

	def checkCheckSumm(self , md5 ):
		if md5.search( md5.lastmd5Summ ):
			self.fs.remInputFile(self.processedFile)
			return False 

		return True

	def compressFile(self):
		# Сжимаем файл
		try:			
			self.processedFile.zipFile()
			return True
		except:			
			self.fs.remInputFile(self.processedFile)
			self.fs.remOutPutFile(self.processedFile)
			return False

	def checkCompressFile(self):
		# Проверяем сжатого файла
	    if not self.processedFile.iszipFile():
	        self.fs.remInputFile(self.processedFile)
	        self.fs.remOutPutFile(self.processedFile)
	        return False

	    return True

	def removeSrcFile(self):
		tryCount = 0

		if not os.path.exists(os.path.join( self.processedFile.root , self.processedFile.file )):
			return True

		while not self.fs.removeSrcFile(self.processedFile):
			tryCount += 1		    
			time.sleep( self.sleepTryLimit )
			if tryCount > self.tryCountLimit:

				self.fs.remInputFile(self.processedFile)
				self.fs.remOutPutFile(self.processedFile)
				return False

			print( "Попытка № " + str(tryCount) )

		return True

	def uploadCompressedFile(self):
		#	заливаем файл обратно из папки output
		tryCount = 0
		while not self.fs.copyFile( self.processedFile.scriptPath + "/output/" + self.processedFile.file , self.processedFile.filePath ):
			tryCount += 1		    
			time.sleep( self.sleepTryLimit )
			if tryCount > self.tryCountLimit:
				return False

			print( "Попытка № " + str(tryCount) )

		return True

	def saveMD5Self( self ,  md5 ):
		return self.saveMD5_(self.processedFile.scriptPath + "/input/" + self.processedFile.file , md5)

	def saveMD5( self ,  md5 ):
		return self.saveMD5_(self.processedFile.scriptPath + "/output/" + self.processedFile.file , md5)

	def saveMD5_(self , f , md5 ):

		tryCount = 0

		while not md5.get( f ):
			tryCount += 1
			time.sleep( self.sleepTryLimit )

			if tryCount > self.tryCountLimit:

				try:

					os.remove( self.processedFile.filePath )

					if self.fs.copyFile( self.processedFile.scriptPath + "/input/" + self.processedFile.file , self.processedFile.filePath ) == 0:
						self._log.error(" - По какой то причине не удалось получить контрольную сумму сжатого файла, и вернуть обратно исходный, это говорит о серьезной неполадке, программа будет завершена. Исходный файл находиться в папке INPUT вам нужно вренуть его обратно вручную(" + self.processedFile.filePath + ") ВНИМАНИЕ!!! Повторный запуск программы удалит исходный файл"  )
						fs.mkLockFile()
						os._exit(0)


				except:

					self._log.error(" - По какой то причине не удалось получить контрольную сумму сжатого файла и вернуть обратно исходный файл тоже не вышло, это говорит о серьезной неполадке, программа будет завершена. Исходный файл находиться в папке INPUT вам нужно вренуть его обратно вручную(" + self.processedFile.filePath + ") ВНИМАНИЕ!!! Повторный запуск программы удалит исходный файл"  )
					fs.mkLockFile()
					os._exit(0)


			print( "Попытка № " + str(tryCount) )


		md5.add( md5.lastmd5Summ )
		return True



			
           
       
