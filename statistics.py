class myStat:

	def __init__(self , _log ):

		self._log = _log

		# удачно обработанные файлы
		self.filecount = 0

		#	Входящий размер всех удачно обработанных файлов
		self.inputSize = 0

		#	Выходящий размер всех удачно обработанных файлов
		self.outputSize = 0

		#	Всего файлов
		self.allfiles = 0

		#	Файлы которые уже обрабатывались
		self.checkSummDroppedFiles = 0

		# пропущены так как сжатие было не эфективно
		self.compressNotEffective = 0

		# Исключения
		self.exep = 0;

	def startNewFile(self):
		self.allfiles += 1

	def fileCompressGood( self, inputSize , outputSize ):
		self.inputSize += inputSize
		self.outputSize += outputSize
		self.filecount +=1 

	def fileAlreadyCompress(self):
		self.checkSummDroppedFiles += 1	

	def showInfo(self):

		if self.allfiles == 0: 			
			return

		try:
			persent = round( 100 - ((self.outputSize / self.inputSize) * 100), 2)		
		except Exception as e:
			persent = 0			

		allfiles = self.allfiles  - 1

		self.freeSpace = round( (self.inputSize - self.outputSize) / 1024 / 1024 , 2 )

		self.errorFile = allfiles - ( self.filecount + self.checkSummDroppedFiles + self.exep + self.compressNotEffective )

		self._log.infoConsole(str("\n\tВсего обработано: ") + str( allfiles  ) + " Усп.: " + str(self.filecount) + " Не эфф.:" + str( self.compressNotEffective ) +  " Про.: " + str(self.checkSummDroppedFiles) + " Иск.: " + str(self.exep) + " Оши.: " + str( self.errorFile ) +   str(" \n\tИсходный размер файлов: ") + str(round((self.inputSize / 1024 / 1024), 2)) + "Mb"  + str(" Размер после сжатия: ") + str( round((self.outputSize / 1024 / 1024), 2)) + "Mb" + str("\n\tПроцент сжатия: ") + str( persent ) + str("% Освобождено: ") + str( self.freeSpace ) + "Мb" )
	    
		
