import shutil
import os.path
import os
import traceback
import threading
import time

class fsOperations:

    def __init__(  self , _log ):
        self._log = _log
        self.ifShowOk = False
        self.threadStart = False

    def progreesBar( self , name ):

        counter = 0

        while self.threadStart:

            counter = counter + 1

            if counter == 30 :
                 print( "Копирую...", end=" " , flush=True  )
                 self.ifShowOk = True

            if counter >= 30 and counter % 10 == 0:
                try:
                    file_stats = os.stat(self.F)
                    file_stats_ned = os.stat(self.T)
                    print( round( ( file_stats_ned.st_size / file_stats.st_size ) * 100 ), end="% " , flush=True  )
                except:
                    pass

            time.sleep( 0.1 )

    def initTmpDirs( self ):
        # Очистим временные каталоги
        pathScript, filename = os.path.split(os.path.abspath(__file__))
        try:
            shutil.rmtree( pathScript + "/input/")
            shutil.rmtree( pathScript + "/output/")
        except:
            pass
        os.mkdir( pathScript + "/input/")
        os.mkdir( pathScript + "/output/")

    def checkFileSize( self, form, to  ):
        try:
            file_stats = os.stat(form)
            file_stats_ned = os.stat(str(to))
            #print( file_stats.st_size  )
            #print( file_stats_ned.st_size  )
            if file_stats.st_size == file_stats_ned.st_size:
                return 1
            else:
                self._log.error(' - ОШИБКА во время копирования, размер не совпадает ')
                return 0
        except:
            self._log.error(' - ОШИБКА во время копирования, не удалось сравнить размеры файлов\n' + traceback.format_exc() )
            return 0

    def copyFile( self, form, to ):
        
        self.F = form
        self.T = to

        self._log.info( 'Копирую: \n из ' + str(form) + " в " + str(to) )

        try:
            self.x = threading.Thread( target=self.progreesBar, args=(1,) )
            self.x.daemon = True
            self.threadStart = True;
            self.x.start()

            shutil.copy( str(form), str(to) )
            if self.ifShowOk:
                print(" OK")

        except:

            #   Инога бывает косяк с правами. Файл скопирвоан но почему то возвращаеться исключение
            #   Првоерим вдруг мы ег восетаки скоприовали
            if self.checkFileSize( form, to ):
                self.threadStart = False
                self._log.info(' + Скопирован успешно( с исключением )')
                return 1

            self._log.error(' - ОШИБКА во время копирования:\n' + traceback.format_exc() )
            self.threadStart = False
            return 0

        if not os.path.exists(str(to)):
            self._log.error(' - ОШИБКА во время копирования, файл не найден')
            self.threadStart = False
            return 0

        if self.checkFileSize( form, to ):
            self.threadStart = False
            self._log.info(' + Скопирован успешно')
            return 1

        return 0


    def remInputFile( self , file ):
        try:
            os.remove( file.scriptPath + "/input/" + file.file )
        except:
            pass
            

    def remOutPutFile( self,  file ):
        try:
            os.remove( file.scriptPath + "/output/" + file.file)
        except:
            pass	

    def removeSrcFile(self , file):
       
        try:
            os.remove(file.filePath)
        except:
            return False

        return True

    def mkLockFile():
        my_file = open("lock", "w+")
        my_file.close()
