import time
import threading
import os
import signal

class saveExitClass:

    def __init__(  self ):

        signal.signal( signal.SIGINT, self.handler ) # ctlr + c
        signal.signal( signal.SIGTSTP, self.handler ) # ctlr + z

        self.isCanExitVal = True;


    # поток для корректного завершения программы
    def thread_function( self, name):
        while True:
            print(self.isCanExitVal)
            if self.isCanExitVal:
                print("Программа успешно завершена")
                os._exit(1)
            print('Завершение сейчас не возможно идет критическая операция. Ожидаем завершения операции...')
            time.sleep(1)

    def handler( self, signal_received, frame):
        # Handle any cleanup here
        print('\nИнициализирован выход прраммы... нужно првоерить можно ли сейчас завершить работу')
        x = threading.Thread(target=self.thread_function, args=(1,))
        x.daemon = True
        x.start()

    def isCanExit( self, value ):
        self.isCanExitVal = value






