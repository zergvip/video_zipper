import logging
import time
import shutil
import os
import traceback
import sys

class processedFile:

    def __init__( self ):
        pass

    def setFile(self , root ,  file ):
        self.root = root
        self.file = file

        self.filePath = os.path.join( root , file)
        self.scriptPath, f = os.path.split(os.path.abspath(__file__))

    def setLog( self , _log ):
        self._log = _log     

    def setStat( self , stat):
        self.stat = stat    

    def getFileSize( self ):
        try:
            self.srcStats = os.stat(self.filePath)
        except:
            self._log.error(' - ОШИБКА при получении размера файла\n' + traceback.format_exc() + "\n")
            return -1

        return self.srcStats.st_size

    def compareFiles(self):

        try:

            inputStats = os.stat( self.scriptPath + "/input/" + self.file )
            outputStats = os.stat( self.scriptPath + "/output/" + self.file )

            if inputStats.st_size > outputStats.st_size:
                return True

            return False

        except:
            self._log.error(' - ОШИБКА при сравнении размеров файлов, не удалось получить размер (compareFiles)')
            return False

    def calulateSizes(self):
        try:

            self.inputStats = os.stat( self.scriptPath + "/input/" + self.file)
            self.outputStats = os.stat( self.scriptPath + "/output/" + self.file)

            self._log.infoConsole( str("Исх.: ") + str( round(self.inputStats.st_size / 1024 / 1024 , 2 ) ) + str("Mb Сж.: ") + str(round( self.outputStats.st_size / 1024 / 1024 , 2 ) ) + "Mb (" + str( round( (100 - (( self.outputStats.st_size / self.inputStats.st_size ) * 100) ) , 2 ) ) + str("%)")  )   

            return True         

        except Exception as e:
            print(e)
            os._exit(1)
            self._log.error(' - ОШИБКА при отображении размеров файлов, не удалось получить размер (calulateSizes)')
            return False