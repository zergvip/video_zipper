import hashlib
import logging
import os
from datetime import datetime
import shutil

class myLog:

    def __init__(self):

        self.scriptPath, f = os.path.split(os.path.abspath(__file__))

        # Создаем папку лога если нет
        pathScript, filename = os.path.split(os.path.abspath(__file__))
        if not os.path.exists( pathScript + "/logs/" ):
            os.mkdir(pathScript + "/logs/")

        self.loggerInfo = logging.getLogger( "vidoe_zipper_info"  )
        self.loggerInfo.setLevel(logging.INFO)

        fh =  logging.FileHandler( self.scriptPath + str("/logs/") + str(datetime.now().strftime("%Y-%m-%d %H-%M-%S")) + "_INFO.log" , encoding = "UTF-8" )
        formatter =  logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        self.loggerInfo.addHandler(fh)

        '''self.loggerError = logging.getLogger("vidoe_zipper_error" )
        self.loggerError.setLevel(logging.ERROR)

        fh = logging.FileHandler( self.scriptPath + str("/logs/") + str(datetime.now().strftime("%Y-%m-%d %H-%M-%S")) + "_ERRORS.log" , encoding = "UTF-8")
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        self.loggerError.addHandler(fh)'''

    def info(self , text):
        self.loggerInfo.info(text)

    def infoConsole(self , text):
        print(text)
        self.loggerInfo.info(text)

    def error(self , text):
        print(text)
        self.loggerInfo.error(text)
