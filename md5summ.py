import hashlib
import logging
import os
import traceback

class md5summ:

    def __init__(self,  _log):
        self._log = _log
        self.scriptPath, f = os.path.split(os.path.abspath(__file__))

        if not os.path.exists( self.scriptPath + '/checksum.txt' ):
            f = open( self.scriptPath + '/checksum.txt', 'w+', encoding='utf-8' )
            f.close()

        #   Обработаем дубликаты если они есть
        os.system('awk \'!seen[$0]++\' checksum.txt > checksum.txt_tmp')

        if not os.path.exists( self.scriptPath + '/checksum.txt_tmp' ):
            self._log.error(" - ОШИБКА при удалении дубликатов контрольхы сумм. Временный файл суммы не удалось создать")
            os._exit(0)

        os.system('rm checksum.txt')
        os.system('mv checksum.txt_tmp checksum.txt')


    def add( self , summ):

        if self.search( summ ):
            return True

        self.md5Data.add(summ)

        f = open(self.scriptPath + '/checksum.txt', 'a', encoding='utf-8')
        f.write( self.lastmd5Summ + "\n")
        f.close()

    def load(self):
        fp = open( self.scriptPath + '/checksum.txt', 'r', encoding='utf-8')
        self.md5Data = set( fp.readlines() )

    def get( self, fname ):
        hash_md5 = hashlib.md5()
        try:
            with open(fname, "rb") as f:
                for chunk in iter(lambda: f.read(4096), b""):
                    hash_md5.update(chunk)
        except:
            self._log.error(" - ОШИБКА получения контрольной суммы:\n" + traceback.format_exc() + "\n")
            return False
        self.lastmd5Summ = hash_md5.hexdigest()
        return True

    def search( self , checkSummCurrent ):
        if checkSummCurrent + "\n" in self.md5Data:
            return 1
        return 0
